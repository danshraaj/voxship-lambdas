'use strict';
let mysql = require('mysql');
const req = require("request");

let pool = mysql.createPool({
        host: process.env.host,
        user: process.env.user,
        password: process.env.password,
        database : process.env.database,
        port     : 3306,
        queueLimit : 0, // unlimited queueing
        connectionLimit : 0 // unlimited connections 
    });
    
const fedexAPI = require("./lib/index.js");
const fedex = new fedexAPI({
  account_number: "510087500",
  meter_number: "118779952",
  key: "rn2pqSd8c4DilLXB",
  password: "zy1V94x9u1Hm45UUB01QSCn2J",
  env: "test"
});

var date = new Date();

exports.handler = async (event, context, callback) => {
    //prevent timeout from waiting event loop
    context.callbackWaitsForEmptyEventLoop = false;
    
    let messageBody = JSON.parse(event.Records[0].body);
  
    let batchNumberId = messageBody.batchNumberId;
    let companyName = messageBody.companyName;
    let orderId = messageBody.orderId;
    let orderNumber = messageBody.orderNumber;
    let boxNumber = messageBody.boxNumber;
    
    let boxWeight = messageBody.packageDetails.boxWeight;
    let boxLength = messageBody.packageDetails.boxLength;
    let boxWidth = messageBody.packageDetails.boxWidth;
    let boxHeight = messageBody.packageDetails.boxHeight;
    let shippingCompany = messageBody.shippingCompany;
    let shippingMethod = messageBody.shippingMethod;
    let shippingCountry = messageBody.orderShippingAddress.country;
    let userId = messageBody.userId;
    let customerId = messageBody.customerId;
    
    if(shippingCompany == "FEDEX") // fedex shipment
    {
      let servicePackage = messageBody.servicePackage || "YOUR_PACKAGING";
  
      let shipper = {
        "Contact": {
          "PersonName": messageBody.companyFromAddress.companyName,
          "CompanyName": companyName,
          "PhoneNumber": messageBody.companyFromAddress.companyPhone || ""
        },
        "Address": {
          "StreetLines": messageBody.companyFromAddress.companyAddress1+" "+messageBody.companyFromAddress.companyAddress2 || "",
          "City": messageBody.companyFromAddress.companyCity,
          "StateOrProvinceCode": messageBody.companyFromAddress.companyState,
          "PostalCode": messageBody.companyFromAddress.companyZip,
          "CountryCode": messageBody.companyFromAddress.companyCountry
        }
      };
      let recipient = {
        "Contact": {
          "PersonName": messageBody.orderShippingAddress.shipTo,
          "CompanyName": messageBody.orderShippingAddress.customerOfficeName || "",
          "PhoneNumber": messageBody.orderShippingAddress.phone1 || ""
        },
        "Address": {
          "StreetLines": messageBody.orderShippingAddress.address1+" "+messageBody.orderShippingAddress.address2 || "",
          "City": messageBody.orderShippingAddress.city,
          "StateOrProvinceCode": messageBody.orderShippingAddress.state,
          "PostalCode": messageBody.orderShippingAddress.zip,
          "CountryCode": messageBody.orderShippingAddress.country,
          "Residential": messageBody.addressStatus
        }
      };
      let shippingChargesPayment = {
        "PaymentType": "THIRD_PARTY", // SENDER, THIRD_PARTY
        "Payor": {
            "ResponsibleParty": {
                "AccountNumber": fedex.options.account_number,
                "Contact": "",
                "Address": {
                  "CountryCode": "US"
                }
            }
        }
      };
      let labelSpecification = {
        "LabelFormatType": "COMMON2D",
        "ImageType": "ZPLII",
        "LabelStockType": "STOCK_4X6"
      };
      let requestedPackageLineItems = [{
        "SequenceNumber": 1,
        "GroupPackageCount": 1,
        "Weight": {
          "Units": "LB",
          "Value": parseInt(boxWeight, 10)
        },
        "Dimensions": {
          "Length": parseFloat(boxLength),
          "Width": parseFloat(boxWidth),
          "Height": parseFloat(boxHeight),
          "Units": "IN"
        },
        "CustomerReferences": [
          {
            "CustomerReferenceType": "CUSTOMER_REFERENCE",
            "Value": companyName
          },
          {
            "CustomerReferenceType": "INVOICE_NUMBER",
            "Value": orderNumber
          },
          {
            "CustomerReferenceType": "P_O_NUMBER",
            "Value": boxNumber
          }
        ]
      }];
  
      if(shippingCountry == "US") 
      {
        let params = {
          "RequestedShipment": {
            "ShipTimestamp": new Date(date.getTime() + (24*60*60*1000)).toISOString(),
            "DropoffType": "REGULAR_PICKUP",
            "ServiceType": shippingMethod,
            "PackagingType": servicePackage,
            "Shipper": shipper,
            "Recipient": recipient,
            "ShippingChargesPayment": shippingChargesPayment,
            "LabelSpecification": labelSpecification,
            "PackageCount": "1",
            "RequestedPackageLineItems": requestedPackageLineItems
          }
        };

        // console.log("=== Start Ship Order # "+orderId+"==="); 
        var res = await fedexShipFun(params);
        // console.log(res)
        if(res)
        {
            if((res.HighestSeverity != "FAILURE") && (res.HighestSeverity != "ERROR")) 
            {
              // console.log("=== Fedex Shipment Created For Order # "+orderId+"===");
              if (typeof res.CompletedShipmentDetail !== 'undefined') {
                let trackingCode = res.CompletedShipmentDetail.CompletedPackageDetails[0].TrackingIds[0].TrackingNumber;
                let shippingCost = 0.00; 
                let shippingLabel = res.CompletedShipmentDetail.CompletedPackageDetails[0].Label.Parts[0].Image;
                let shippingLabelDecodedData = Buffer.from(shippingLabel, 'base64').toString('ascii');
                
                // update bulk_shipment_table
                var sql = "UPDATE bulk_shipment_status SET returnType = 'success', returnCode = '200', message = 'Shipping label created successfully.', trackingCode = '"+trackingCode+"', shippingCost = '"+shippingCost+"', shippingLabel = \""+shippingLabelDecodedData+"\", updatedBy = '"+userId+"' WHERE boxNumber = '"+ boxNumber+"'";
                await performDBQuery(sql,0);
                // update boxes
                var sql = "UPDATE boxes SET trackingCode = '"+trackingCode+"', shippingCost = '"+shippingCost+"', shippingLabel = \""+shippingLabelDecodedData+"\", updatedBy = '"+userId+"' WHERE boxNumber = '"+ boxNumber+"'";
                await performDBQuery(sql,0);
                // update order status
                var sql = "UPDATE orders SET orderStatus = '6' WHERE id = "+ orderId;
                await performDBQuery(sql,0);
              }
              else
              {
                var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = 'Invalid Dimensions' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
                await performDBQuery(sql,0);
              }
            }
            else
            {
              // console.log("=== Error Order # "+orderId+"===");
              
             var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = '"+res.Notifications[0].Message+"' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
              await performDBQuery(sql,0);
            }
        } 
        else
        {
          // console.log("=== Fedex API Error For Order # "+orderId+"===");
          
          var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = 'Error From Fedex api' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
          await performDBQuery(sql,0);
        }
        
        // console.log("=== Finished Order # "+orderId+"===");
      }
      callback(null, "shipment status saved.");
    }
    else if(shippingCompany == "ESSENTIALHUB") // essential hub shipment
    {
      let serviceDetails = shippingMethod.split("-");
      let serviceId = serviceDetails[1];
      let servicePackage = messageBody.servicePackage || "Parcel";
  
      let fromLocation = {
        "company": companyName,
        "first_name": "",
        "last_name": "",
        "address1": messageBody.companyFromAddress.companyAddress1,
        "address2": messageBody.companyFromAddress.companyAddress2 || "",
        "city": messageBody.companyFromAddress.companyCity,
        "state": messageBody.companyFromAddress.companyState,
        "country": messageBody.companyFromAddress.companyCountry,
        "postal_code": messageBody.companyFromAddress.companyZip,
        "phone": messageBody.companyFromAddress.companyPhone || "",
        "email": ""
      };

      let toLocation = {
        "company": messageBody.orderShippingAddress.customerOfficeName || "",
        "first_name": messageBody.orderShippingAddress.shipTo,
        "last_name": "",
        "address1": messageBody.orderShippingAddress.address1,
        "address2": messageBody.orderShippingAddress.address2 || "",
        "city": messageBody.orderShippingAddress.city,
        "state": messageBody.orderShippingAddress.state,
        "country": messageBody.orderShippingAddress.country,
        "postal_code": messageBody.orderShippingAddress.zip,
        "phone": messageBody.orderShippingAddress.phone1 || "",
        "email": ""
      };

      let parcels = [{
        "length": parseInt(boxLength, 10),
        "width": parseInt(boxWidth, 10),
        "height": parseInt(boxHeight, 10),
        "weight": parseInt(16)*parseFloat(boxWeight, 10),  // weight in Oz 
        "package_type": servicePackage,
        /*"service_options": {
          "delivery_confirmation": "NO_SIGNATURE_REQUIRED",
          "priority_handling": "SERVICE_DEFAULT"
        }*/
      }];
      // console.log(parcels); 

      let shipData = {
        "shipment": {
          "to_location": toLocation,
          "from_location": fromLocation,
          "parcels": parcels,
          "service_id": serviceId,
          "label_format": "zpl",
          //label_size: "4x6",
          "label_text1": orderNumber,
          //label_text2: "",
        }
      };

      // call api to save shipment status
      // let resParams = {
      //   url: "https://api.essentialhub.com/api/v2/shipments/ship",
      //   headers: { "Content-Type": "application/json", "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Mzg2OTIyOTQsImRhdGEiOnsidXNlciI6eyJpZCI6MTEwLCJjdXN0b21lcl9pZCI6ODgsImVtYWlsIjoidm94bWFya2V0aW5nZ3JvdXBAZXNzZW50aWFsaHViLmNvbSJ9LCJzY29wZXMiOlsiYXBpX3B1YmxpYyJdfX0.psGQqJzl3FkVzuaKzLbj0UeSpN8qe34dysuR_YDIY1UOebdLVVtuTaSTBdkj8H4zJHcOPs64gqUx79i9qi5lAA" },
      //   json: shipData
      // };
      
      if(customerId == 1)
      {
        // test account
        var resParams = {
            url: "https://api.essentialhub.com/api/v2/shipments/ship",
            headers: { "Content-Type": "application/json", "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MzE4NTA1ODcsImRhdGEiOnsidXNlciI6eyJpZCI6NzQsImN1c3RvbWVyX2lkIjo1OSwiZW1haWwiOiJyaWNreUB2b3htZy5jb20ifSwic2NvcGVzIjpbImFwaV9wdWJsaWMiXX19.nE6OTfSZ5aF1DuA9uR5DUJo0umBQxMSuVAKgRi4Stu2SufmaghW8vbS5l44NJmg4kmZZLCNaarxGuoPrEVmn8w" },
            json: shipData
          };
      }
      else
      {
        // live account
        var resParams = {
            url: "https://api.essentialhub.com/api/v2/shipments/ship",
            headers: { "Content-Type": "application/json", "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Mzg2OTIyOTQsImRhdGEiOnsidXNlciI6eyJpZCI6MTEwLCJjdXN0b21lcl9pZCI6ODgsImVtYWlsIjoidm94bWFya2V0aW5nZ3JvdXBAZXNzZW50aWFsaHViLmNvbSJ9LCJzY29wZXMiOlsiYXBpX3B1YmxpYyJdfX0.psGQqJzl3FkVzuaKzLbj0UeSpN8qe34dysuR_YDIY1UOebdLVVtuTaSTBdkj8H4zJHcOPs64gqUx79i9qi5lAA" },
            json: shipData
          };
      }
      
      var res = await essentialHubShipFun(resParams);
      
      if(res)
      {
        if(res.body.status != "error" && res.body.shipment)
        {
          // console.log("===Essential Hub Shipment Created For Order # "+orderId+"==="); 
          let trackingCode = res.body.shipment.tracking_number;
          let shippingCost = res.body.shipment.shipping_service.rate;
          let shippingLabel = res.body.shipment.parcels[0].postage_label.image_url;
          // console.log(shippingLabel)
          let options = {
            uri: shippingLabel,
            encoding: null
          };
          
          var res = await essentialHubShipLabelFun(options);

          if(res) {
            var shippingLabelDecodedData = Buffer.from(res, 'base64').toString('ascii');
            
            // update bulk_shipment_table
              var sql = "UPDATE bulk_shipment_status SET returnType = 'success', returnCode = '200', message = 'Shipping label created successfully.', trackingCode = '"+trackingCode+"', shippingCost = '"+shippingCost+"', shippingLabel = \""+shippingLabelDecodedData+"\", updatedBy = '"+userId+"' WHERE boxNumber = '"+ boxNumber+"'";
              await performDBQuery(sql,0);
              // update boxes
              var sql = "UPDATE boxes SET trackingCode = '"+trackingCode+"', shippingCost = '"+shippingCost+"', shippingLabel = \""+shippingLabelDecodedData+"\", updatedBy = '"+userId+"' WHERE boxNumber = '"+ boxNumber+"'";
              await performDBQuery(sql,0);
              // update order status
              var sql = "UPDATE orders SET orderStatus = '6' WHERE id = "+ orderId;
              await performDBQuery(sql,0);
            
          } else {
            var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = 'Error Getting Zpl data.' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
            await performDBQuery(sql,0);
          }
        }
        else
        {
          var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = '"+res.body.error_message+"' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
          await performDBQuery(sql,0);
        }
          
      }
      else
      {
        // save api call error here
        var sql = "UPDATE bulk_shipment_status SET returnType = 'error', returnCode = '202', message = 'Error Connecting EssentialHub Api' WHERE batchNumberId = "+ batchNumberId +" AND orderId = "+ orderId;
        await performDBQuery(sql,0);
      }
      
      callback(null, "shipment status saved.");
  }
  
};

let performDBQuery = async (sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) { reject(err); }
            connection.query(sql, params, (err, results) => {
                if (err) { reject(err); }
                connection.release();
                resolve(results);
            });
        });
    });
};

let fedexShipFun = async (params) => {
    return new Promise((resolve, reject) => {
        fedex.ship(params, (err, res) => {
            if (err) { resolve(err); }
            resolve(res);
        });
    });
};

let essentialHubShipFun = async (resParams) => {
    return new Promise((resolve, reject) => {
        req.post(resParams, (err, res) => {
            if (err) { resolve(err); }
            resolve(res);
        });
    });
};

let essentialHubShipLabelFun = async (options) => {
    return new Promise((resolve, reject) => {
        req(options, (err, res, body) => {
            if (err || res.statusCode !== 200) { resolve(err); }
            resolve(body);
        });
    });
};
