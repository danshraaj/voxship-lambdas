let req = require("request");
exports.handler = (event, context, callback) => {
	//console.log('event', JSON.stringify(event) );
	let messageBody = event;
	//let messageBody = JSON.parse(event.body);
	//console.log("messageBody", messageBody);
	if (messageBody.shippingCompanies.indexOf("ESSENTIALHUB") > -1) {
		let ESSENTIALHUB = [];

		let fromLocation = {
			"company": "",
			"first_name": "",
			"last_name": "",
			"address1": "",
			"address2": "",
			"city": "",
			"state": "",
			"country": messageBody.companyFromAddress.companyCountry,
			"postal_code": messageBody.companyFromAddress.companyZip,
			"phone": "",
			"email": ""
		};

		let toLocation = {
			"company": "",
			"first_name": "",
			"last_name": "",
			"address1": messageBody.shippingAddress.address1 || "",
			"address2": messageBody.shippingAddress.address2 || "",
			"city": messageBody.shippingAddress.city,
			"state": messageBody.shippingAddress.state,
			"country": messageBody.shippingAddress.country,
			"postal_code": messageBody.shippingAddress.zip,
			"phone": "",
			"email": ""
		};

		let parcels = [{
			/*"length": parseInt(boxLength, 10),
			"width": parseInt(boxWidth, 10),
			"height": parseInt(boxHeight, 10),*/
			"weight": parseInt(16)*parseFloat(messageBody.weight),  // weight in Oz
		}];

		let rateData = {
			"shipment": {
				"to_location": toLocation,
				"from_location": fromLocation,
				"parcels": parcels
			}
		};

		// call api to save shipment status
		let resParams = {
			url: "https://api.essentialhub.com/api/v2/rates",
			headers: { "Content-Type": "application/json", "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MzE4NTA1ODcsImRhdGEiOnsidXNlciI6eyJpZCI6NzQsImN1c3RvbWVyX2lkIjo1OSwiZW1haWwiOiJyaWNreUB2b3htZy5jb20ifSwic2NvcGVzIjpbImFwaV9wdWJsaWMiXX19.nE6OTfSZ5aF1DuA9uR5DUJo0umBQxMSuVAKgRi4Stu2SufmaghW8vbS5l44NJmg4kmZZLCNaarxGuoPrEVmn8w" },
			json: rateData
		};

		//console.log("res params", resParams);
		try {
			req.post(resParams, function(err, res, body) {
				if(err) {
					//console.log("Essential Hub API Error: ", err);
					callback(null, err);
				}
	
				//console.log('body', body);
				if(body && body.service_rates) {
					//console.log("===Essential Hub Rates===");
					if(Array.isArray(body.service_rates)) {
						body.service_rates.forEach(rate => {
							//console.log('rate', rate);
							ESSENTIALHUB.push({
								shippingMethod: rate.service,
								serviceType: rate.service_id,
								packageType: "",
								cost: rate.rate,
								deliverDays: rate.delivery_days,
								deliveryDate: rate.delivery_date
							});
						});
						//console.log("Rate Shopping", ESSENTIALHUB);
						callback(null, {ESSENTIALHUB});
					} else {
						//console.log("Rate response not array");
						callback(null, "Rate response not array.");
					}
					
				} else if(body && body.status == "error") {
					//console.log("Essential Hub Status Error:", body.error_message);
					callback(null, body.error_message);
				} else {
					//console.log("===Essential Hub Error===");
					callback(null, "Essential Hub Error.");
				}
			});
		} catch(err) {
			callback(null, err);
		}
		
	} else {
		callback(null, "Essential Hub not found in shipping companies");
	}
};

