// stamps shipment
let Stamps = require("stamps-api");
let stampsCred = {
  "id": "d48aa4a6-94ff-492d-87b5-661c0f36fcdc",
  "username": "zummix2014",
  "password": "zummix1"
};

exports.handler = (event, context, callback) => {
  //console.log('event', JSON.stringify(event) );
  let messageBody = event;
  //let messageBody = JSON.parse(event.body);
  //console.log("messageBody", messageBody);
  if (messageBody.shippingCompanies.indexOf("STAMPS") > -1) {
    let STAMPS = [];

    let rateParams = {
      "FromZIPCode": messageBody.companyFromAddress.companyZip,
      "ToZIPCode": messageBody.shippingAddress.zip,
      "ShipDate": new Date().toISOString().split("T")[0]
    };

    /*var rateParams = {
        FromZIPCode: "95060",
        ToZIPCode: "95060",
        ServiceType: "US-FC",
        PrintLayout: "Envelope9",
        WeightLb: 0,
        WeightOz: 1,
        PackageType: "Letter",
        ShipDate: new Date().toISOString().split('T')[0],
        RectangularShaped: true
    };*/

    //console.log('rate params', rateParams);
    Stamps.connect().then(() => {
      Stamps.auth(stampsCred).then(() => {
        // stamps get rates
        Stamps.request("GetRates", {
          "Rate": rateParams
        })
        .then((ratesRes) => {
          //console.log('rates response', ratesRes);
          if(ratesRes && ratesRes.Rates) {
            //console.log("===Stamps rate shopping===");
            if(Array.isArray(ratesRes.Rates.Rate)) {
              ratesRes.Rates.Rate.forEach(rate => {
                STAMPS.push({
                  shippingMethod: "",
                  serviceType: rate.ServiceType,
                  packageType: rate.PackageType,
                  cost: rate.Amount,
                  deliverDays: rate.DeliverDays,
                  deliveryDate: rate.DeliveryDate
                });
              });
              //console.log("Rate Shopping", STAMPS);
              callback(null, {STAMPS});
              
            } else {
              //console.log("Rate response not array");
              callback(null, "Rate response not array");
            }
          } else {
            //console.log("Rate Shopping Error Response: ", ratesRes);
            callback(null, ratesRes);
          }
        })
        .catch(err => {
          //console.log("Stamps API Error: ", err);
          callback(null, err);
        });
      })
      .catch(err => {
        //console.log("===Stamps Auth API Error: ", err);
        callback(null, err);
      });
    })
    .catch(err => {
      //console.log("===Stamps Connect API Error: ", err);
      callback(null, err);
    });
  } else {
    callback(null, "Stamps not found in shipping companies");
  }
  
};

